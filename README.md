# README #

Este arquivo contempla uma explicação sobre o projeto calc-juros.

### Objetivo do Projeto ###

Este é um projeto Java EE (web), nomeado "calc-juros", tem como objetivo simular o cadastro de clientes em uma base de dados, e estes clientes cadastrados poderem realizar simulações de contratação de algum empréstimo bancário.
Nesta simulação de empréstimo, o cliente informa o valor a ser contratado e o tempo para amortizar o pagamento, com estas informações, o resultado é apresentado na tela.

### Tecnologias utilizadas ###

Para o desenvolvimento deste projeto, foi utilizada a linguagem de programação Java versão 8, e as principais tecnologias e frameworks utilizados foram:
Para o desenvolvimento das telas (front-end) foi utilizado o JSF com Primefaces e arquivos no formato XHTML.
Para a camada back-end, foi utilizado JPA (mapeamento das entidades), EJB e CDI para interligar as camadas da aplicação junto ao servidor de aplicação.
Os testes das regras de negócio foram realizados com JUnit e Mockito.
Foi utilizado o Maven para a construção do projeto e gerenciamento das dependências.
O servidor de aplicação escolhido foi o WildFly versão 11.

### Arquitetura ###

Uma arquitetura simples foi montada para o desenvolvimento deste projeto, por isso, aqui se explica os principais pacotes e camadas.
Para realizar a conexão com a interface (telas do JSF), foi criado o pacote "mb" que contém o ManagedBean (controller) gerenciado pelo CDI.
Dentro do pacote "business", estão as classes EJB que realizam as ações de regras de negócio necessárias.
Em "model", estão as entidades mapeadas pelo JPA.
Em "persistence", estão as classes responsáveis pela conexão com o Banco de Dados.
Para facilitar os testes externos, não foi utilizado banco de dados, o mesmo é simulado por uma lista contida no pacote "factory" e que é gerenciada pelo WildFly.
Esta lista permanecerá ativa na sessão durante o ciclo de vida da aplicação. Porém, para quem deseja simular com um banco de dados real, os scripts de criação da base e tabela estão disponibilizados no pacote "sql".
Em "util", estão as classes utilitárias do sistema, como uma para auxiliar o JSF na criação de mensagens e também outra para ler o arquivo de internacionalização com as mensagens que está dentro da raíz de "resources".

### Apresentação da interface ###

O sistema apresenta um menu superior com a opção de entrada para Clientes. O usuário é redirecionado para a listagem de clientes, que será atualizada a cada cliente inserido.
Dentro dessa tela, o usuário possui as opções de incluir, editar ou excluir um Cliente. Há também a opção para acessar a tela de simulação de empréstimo.

### Executar a aplicação ###

Para executar e testar a aplicação, requer que o Java 8 ou superior esteja instalado e também o WildFly 11.0.0.
Certifique-se que o WildFly esteja configurado corretamente e sendo executado, para então testar em seu navegador o sistema, acessando a url <http://localhost:8080/calc-juros>.
