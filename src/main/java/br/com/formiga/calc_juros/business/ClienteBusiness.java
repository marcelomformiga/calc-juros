
package br.com.formiga.calc_juros.business;


import br.com.formiga.calc_juros.business.bo.ClienteBO;
import br.com.formiga.calc_juros.enumeration.RiscoEnum;
import br.com.formiga.calc_juros.model.Cliente;
import br.com.formiga.calc_juros.model.dto.EmprestimoDTO;
import java.util.Objects;
import javax.ejb.Stateless;


/**
 * Classe para manter as regras de negócio do controlador Cliente.
 * 
 * @author formiga
 */
@Stateless(name = "clienteBusiness")
public class ClienteBusiness implements ClienteBO {

    @Override
    public Character mapearRisco(final Cliente cliente) {
        
        Character risco = null;
        
        if (Objects.nonNull(cliente.getRendimentoMensal())) {
            
            if (cliente.getRendimentoMensal() <= 2000D) {
                risco = RiscoEnum.C.getRISCO();
            } else if (cliente.getRendimentoMensal() <= 8000D) {
                risco = RiscoEnum.B.getRISCO();
            } else {
                risco = RiscoEnum.A.getRISCO();
            }
        }
        
        return risco;
    }

    @Override
    public Double recuperarTaxa(final Cliente cliente) {
        
        Double taxa = null;
        
        if (Objects.nonNull(cliente.getRisco())) {
            
            switch (cliente.getRisco()) {
                
            case 'A':
                taxa = RiscoEnum.A.getTAXA();
                break;
                
            case 'B':
                taxa = RiscoEnum.B.getTAXA();
                break;
                
            default:
                taxa = RiscoEnum.C.getTAXA();
                break;
            }
        }
        
        return taxa;
    }

    @Override
    public EmprestimoDTO calcular(final EmprestimoDTO emprestimo) {
        
        final Double jurosMes = (emprestimo.getValorSolicitado() * emprestimo.getTaxa()) / 100;
        
        final Double jurosTotal = jurosMes * emprestimo.getTempo();
        
        final Double total = emprestimo.getValorSolicitado() + jurosTotal;

        EmprestimoDTO emprestimoAtualizado = emprestimo;
        emprestimoAtualizado.setJurosMes(jurosMes);
        emprestimoAtualizado.setJurosTotal(jurosTotal);
        emprestimoAtualizado.setValorTotal(total);
        
        return emprestimoAtualizado;
    }
    
}