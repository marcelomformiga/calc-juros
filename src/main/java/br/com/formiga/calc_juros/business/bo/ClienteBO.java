
package br.com.formiga.calc_juros.business.bo;


import br.com.formiga.calc_juros.model.Cliente;
import br.com.formiga.calc_juros.model.dto.EmprestimoDTO;
import javax.ejb.Local;


/**
 * Interface para a classe ClienteBusiness
 * 
 * @author formiga
 */
@Local
public interface ClienteBO {
    
    public abstract Character mapearRisco(final Cliente cliente);
    
    public abstract Double recuperarTaxa(final Cliente cliente);
    
    public abstract EmprestimoDTO calcular(final EmprestimoDTO emprestimo);
    
}