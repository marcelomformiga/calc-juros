
package br.com.formiga.calc_juros.converter;


import br.com.formiga.calc_juros.model.Cliente;
import java.util.Map;
import java.util.Objects;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import org.apache.commons.lang.StringUtils;


/**
 * Classe do tipo Converter para conversão de um "Cliente".
 * 
 * @author formiga
 */
@FacesConverter(value = "clienteConverter", forClass = Cliente.class)
public class ClienteConverter implements Converter {
    
    /**
     * Método para converter uma String em um objeto do tipo "Cliente".
     * 
     * @since 1.1
     * @param context : FacesContext
     * @param component : UIComponent
     * @param value : String
     * @return objeto : Object
     */
    @Override
    public Object getAsObject(final FacesContext context, final UIComponent component, final String value) {
        
        Object objeto = null;
        
        if (Objects.nonNull(value)) {
            objeto = this.getAttributesFrom(component).get(value);
        }
       
        return objeto;
    }

    /**
     * Método para converter um objeto do tipo "Cliente" em uma String.
     * 
     * @since 1.1
     * @param context : FacesContext
     * @param component : UIComponent
     * @param value : Object
     * @return valor : String
     */
    @Override
    public String getAsString(final FacesContext context, final UIComponent component, final Object value)
    {
        String valor = "";
        
        if ((Objects.nonNull(value)) && (StringUtils.isNotBlank(value.toString()))) {
            
            Cliente cliente = (Cliente)value;
            
            if (Objects.nonNull(cliente.getId())) {
                this.addAttribute(component, cliente);
                
                valor = String.valueOf(cliente.getId());
            }
        }
        
        return valor;
    }
    
    /**
     * Método para adicionar um atributo ao componente.
     * 
     * @since 1.1
     * @param component : UIComponent
     * @param paciente : PacienteDTO
     */
    private void addAttribute(final UIComponent component, final Cliente cliente) {
        this.getAttributesFrom(component).put(cliente.getId().toString(), cliente);
    }
 
    /**
     * Método para recuperar os atributos como String.
     * 
     * @since 1.1
     * @param component : UIComponent
     * @return Map dos atributos
     */
    private Map<String, Object> getAttributesFrom(final UIComponent component) {
        return component.getAttributes();
    }
    
}