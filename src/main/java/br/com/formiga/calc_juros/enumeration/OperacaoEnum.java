
package br.com.formiga.calc_juros.enumeration;


import lombok.Getter;


/**
 * Classe Enumeration que contém as operações do Banco de Dados.
 *
 * @author formiga
 */
public enum OperacaoEnum {
    
    /**
     * Chave que represente os valores para incluir um objeto.
     */
    INCLUIR("incluir", "INSERT", "save"),
    
    /**
     * Chave que represente os valores para atualizar um objeto.
     */
    ATUALIZAR("atualizar", "UPDATE", "merge"),
    
    /**
     * Chave que represente os valores para excluir um objeto.
     */
    EXCLUIR("excluir", "DELETE", "delete");

    
    @Getter
    private final String OPERACAO;
    
    @Getter
    private final String SQL;
    
    @Getter
    private final String JPA;
    
    
    /**
     * Construtor que inicializa o valor das constantes.
     */
    OperacaoEnum(final String OPERACAO, final String SQL, final String JPA)
    {
        this.OPERACAO = OPERACAO;
        this.SQL = SQL;
        this.JPA = JPA;
    }
    
}