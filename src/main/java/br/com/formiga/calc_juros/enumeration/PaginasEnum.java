
package br.com.formiga.calc_juros.enumeration;


import lombok.Getter;


/**
 * Enum contendo o caminho das páginas para navegação.
 *
 * @author formiga
 */
public enum PaginasEnum {
    
    CLIENTE_FORMULARIO("formulario"),
    
    CLIENTE_LISTAGEM("listagem"),
    
    EMPRESTIMO_FORMULARIO("emprestimo"),
    
    /**
     * Constante com as informações da página inicial do sistema.
     */
    INDEX("index");
    
    
    /**
     * Atributo que representa o nome da página da constante.
     */
    @Getter
    private final String PAGINA;
    
    
    /**
     * Construtor que inicializa o valor das constantes.
     * 
     * @since 1.0
     */
    PaginasEnum(final String pagina) {
        this.PAGINA = pagina;
    }
    
}