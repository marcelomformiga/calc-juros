
package br.com.formiga.calc_juros.enumeration;


import lombok.Getter;


/**
 * Enumeration para buscar uma mensagem no arquivo de propriedades.
 * 
 * @author formiga
 */
public enum RespostasEnum {
    
    /**
     * Mensagem de sucesso ao excluir um registro.
     * 
     * @since 1.0
     */
    SUCESSO_EXCLUIR("mensagem.sucesso.excluir"),
    
    /**
     * Mensagem de sucesso ao salvar um registro.
     * 
     * @since 1.0
     */
    SUCESSO_SALVAR("mensagem.sucesso.salvar");
    
    
    /**
     * Constante que representa a chave da mensagem do arquivo de propriedades.
     * 
     * @since 1.0
     */
    @Getter
    private final String CHAVE;

    
    /**
     * Construtor inicializando a chave de uma mensagem.
     * 
     * @param chave : String
     */
    RespostasEnum(final String chave) {
        this.CHAVE = chave;
    }
    
}