
package br.com.formiga.calc_juros.enumeration;


import lombok.Getter;


/**
 *
 * @author formiga
 */
public enum RiscoEnum {
    
    A('A', 1.9D),
    B('B', 5.0D),
    C('C', 10.0D);
    
    
    @Getter
    private final Character RISCO;
    
    @Getter
    private final Double TAXA;


    RiscoEnum(final Character risco, final Double taxa) {
        this.RISCO = risco;
        this.TAXA = taxa;
    }
    
}