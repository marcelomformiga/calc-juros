
package br.com.formiga.calc_juros.factory;


import br.com.formiga.calc_juros.model.Cliente;
import br.com.formiga.calc_juros.persistence.repository.ClienteRepository;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.persistence.Transient;
import lombok.Getter;
import lombok.Setter;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;


/**
 * Classe para gerar um LazyDataModel de Paciente.
 * 
 * @author formiga
 */
public class ClienteDataModel extends LazyDataModel<Cliente>
{

    private static final long serialVersionUID = -6719007723105680928L;
    
    /**
     * Atributo que representa o objeto de conexão com o Front-End.
     */
    @Inject
    @Transient
    @Getter
    @Setter
    private Cliente cliente;
    
    /**
     * Lista de Clientes para ser apresentada na tela.
     */
    @Getter
    @Setter
    private List<Cliente> clientes;
    
    /**
     * Atributo responsável por realizar uma chamada aos serviços do domínio de Usuário.
     * 
     * @since 1.1
     */
    @EJB
    @Transient
    @Getter
    private ClienteRepository repository;

    
    /**
     * Construtor padrão.
     */
    public ClienteDataModel() {
        super();
    }
    
    /**
     * Construtor que inicializa a "facade" de acesso ao  repositório de Cliente.
     * 
     * @param repository : ClienteRepository
     */
    public ClienteDataModel(final ClienteRepository repository) {
        super();
        
        this.repository = repository;
    }
    
    /**
     * Construtor para inicializar a lista de Pacientes.
     * 
     * @param clientes : Lista de Clientes
     */
    public ClienteDataModel(final List<Cliente> clientes) {
        this.clientes = clientes;
    }
    
    
    /**
     * Pegar os dados de linha de uma tabela.
     * 
     * @param rowKey : String
     * @return this.cliente : Cliente
     */
    @Override
    public Cliente getRowData(final String rowKey) {
        this.cliente = this.clientes.stream().filter(objeto -> rowKey.equals(objeto.getId().toString())).findFirst().orElse(null);
        
        return this.cliente;
    }
    
    /**
     * Pegar a linha de uma tabela.
     * 
     * @param cliente : Cliente
     * @return Objeto de Cliente
     */
    @Override
    public Object getRowKey(final Cliente cliente) {
        return cliente.getId();
    }
    
    /**
     * Método para listar com paginação de forma "lazy" todos os registros de Cliente.
     * 
     * @param primeiroRegistro : int
     * @param totalDeRegistrosPorPagina : int
     * @param campoOrdenacao : String
     * @param order : SortOrder
     * @param filtros : Map
     * @return 
     */
    @Override
    public List<Cliente> load(final int primeiroRegistro, final int totalDeRegistrosPorPagina, final String campoOrdenacao, final SortOrder order, final Map<String, Object> filtros) {
        
        final boolean ascendente = SortOrder.ASCENDING.equals(order);
        
        this.clientes = this.repository.listar(primeiroRegistro, totalDeRegistrosPorPagina, campoOrdenacao, ascendente, filtros);
        
        return this.clientes;
    }
    
}