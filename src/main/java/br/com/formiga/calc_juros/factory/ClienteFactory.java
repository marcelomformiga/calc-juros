
package br.com.formiga.calc_juros.factory;


import br.com.formiga.calc_juros.model.Cliente;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import lombok.Getter;
import lombok.Setter;


/**
 *
 * @author formiga
 */
@Startup
@Singleton
public class ClienteFactory {
    
    @Getter
    @Setter
    private static List<Cliente> clientes;
    
    @Getter
    @Setter
    private static int id;
    
    
    @PostConstruct  
    public void init() {
    
        ClienteFactory.clientes = new ArrayList<>();
        ClienteFactory.id = 0;
    }
    
}