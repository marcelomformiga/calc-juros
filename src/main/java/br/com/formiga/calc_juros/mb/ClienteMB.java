
package br.com.formiga.calc_juros.mb;


import br.com.formiga.calc_juros.business.bo.ClienteBO;
import br.com.formiga.calc_juros.enumeration.PaginasEnum;
import br.com.formiga.calc_juros.enumeration.RespostasEnum;
import br.com.formiga.calc_juros.enumeration.RiscoEnum;
import br.com.formiga.calc_juros.factory.ClienteDataModel;
import br.com.formiga.calc_juros.factory.ClienteFactory;
import br.com.formiga.calc_juros.model.Cliente;
import br.com.formiga.calc_juros.model.dto.EmprestimoDTO;
import br.com.formiga.calc_juros.persistence.repository.ClienteRepository;
import br.com.formiga.calc_juros.util.BundleUtil;
import br.com.formiga.calc_juros.util.JSFUtil;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import javax.enterprise.context.SessionScoped;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.Transient;
import lombok.Getter;
import lombok.Setter;


/**
 *
 * @author formiga
 */
@Named(value = "clienteMB")
@SessionScoped
public class ClienteMB implements Serializable {

    private static final long serialVersionUID = 8565226857731968996L;
 
    @Getter
    @Setter
    private Cliente cliente;
    
    @Getter
    @Setter
    private List<Cliente> clientes;
    
    @Inject
    @Transient
    @Getter
    private ClienteDataModel dataModel;
    
    @Inject
    @Transient
    @Getter
    private transient ClienteBO clienteBO;
    
    @Inject
    @Transient
    @Getter
    private transient ClienteRepository repository;
    
    @Getter
    @Setter
    private EmprestimoDTO emprestimo;
    
    
    public ClienteMB() {
        this.clientes = ClienteFactory.getClientes();
    }
    
    
    /**
     * Método que salva um Paciente.
     * 
     * @return proximaPagina : String
     */
    public String salvar() {

        this.repository.salvar(this.cliente);
        
        JSFUtil.informarMensagemDeSucesso(BundleUtil.recuperarChave(RespostasEnum.SUCESSO_SALVAR.getCHAVE()));
        
        return PaginasEnum.CLIENTE_FORMULARIO.getPAGINA();
    }
    
    /**
     * Método para editar um Cliente existente.
     * 
     * @return Formulário de edição de um Cliente
     */
    public String editar() {
        
        return PaginasEnum.CLIENTE_FORMULARIO.getPAGINA();
    }
    
    /**
     * Método que exclui um Cliente existente.
     */
    public void excluir() {
        
        this.repository.excluir(this.cliente);
        
        JSFUtil.informarMensagemDeSucesso(BundleUtil.recuperarChave(RespostasEnum.SUCESSO_EXCLUIR.getCHAVE()));
    }
    
    public List<Cliente> listar() {
        
        return this.clientes;
    }
    
    /**
     * Método executado para incializar os objetos da classe.
     * 
     * @return próxima página da aplicação.
     */
    public String inicializarObjetos() {
        
        this.cliente = new Cliente();
        this.clientes = ClienteFactory.getClientes();
        
        return PaginasEnum.CLIENTE_FORMULARIO.getPAGINA();
    }
    
    public void mapearRisco() {
        
        final Character risco = this.clienteBO.mapearRisco(this.cliente);
        
        this.cliente.setRisco(risco);
    }
    
    /**
     * Método para abrir um formulário e o cliente poder simular um empréstimo.
     * 
     * @return formulário para simular empŕestimo
     */
    public String simularEmprestimo() {
        
        final Double taxa = this.clienteBO.recuperarTaxa(this.cliente);
        
        this.emprestimo = new EmprestimoDTO();
        this.emprestimo.setCliente(this.cliente);
        this.emprestimo.setTaxa(taxa);
        
        return PaginasEnum.EMPRESTIMO_FORMULARIO.getPAGINA();
    }
    
    /**
     * Aqui se calcular o empréstimo simulado.
     * 
     * @param actionEvent 
     */
    public void calcular(final ActionEvent actionEvent) {
        
        this.emprestimo = this.clienteBO.calcular(this.emprestimo);
    }   
    
}