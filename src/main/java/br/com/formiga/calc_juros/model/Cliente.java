
package br.com.formiga.calc_juros.model;


import br.com.formiga.calc_juros.enumeration.RiscoEnum;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.validator.constraints.Length;


/**
 *
 * @author formiga
 */
@Entity
@Table(name = "cliente", schema = "calc_juros")
@Data
public class Cliente {
    
    @Column(name = "id", nullable = false, unique = true, length = 20)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    
    @Column(name = "nome", nullable = false, unique = false, length = 80)
    @NotNull(message = "[Nome] não pode ser nulo ou vazio!")
    @Length(min = 1, max = 80, message = "[Nome] deve ter entre {min} e {max} caracteres!")
    private String nome;

    @Column(name = "rendimento_mensal", nullable = false, unique = false, length = 20, columnDefinition = "numeric(10,2)")
    private Double rendimentoMensal;

    @Column(name = "risco", nullable = false, unique = false, length = 1)
    @NotNull(message = "[Risco] não pode ser nulo ou vazio!")
    @Length(min = 1, max = 1, message = "[Risco] deve ter entre {min} e {max} caracteres!")
    private Character risco;

    @Column(name = "endereco", nullable = false, unique = false, length = 200)
    @Length(min = 0, max = 200, message = "Endereço deve ter entre {min} e {max} caracteres!")
    private String endereco;
    
}