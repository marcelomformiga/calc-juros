
package br.com.formiga.calc_juros.model.dto;


import br.com.formiga.calc_juros.model.Cliente;
import lombok.Getter;
import lombok.Setter;


/**
 *
 * @author formiga
 */
public class EmprestimoDTO {
    
    @Getter
    @Setter
    private Cliente cliente;
    
    @Getter
    @Setter
    private Double valorSolicitado;
    
    @Getter
    @Setter
    private Double taxa;
    
    @Getter
    @Setter
    private Integer tempo;
    
    @Getter
    @Setter
    private Double jurosMes;
    
    @Getter
    @Setter
    private Double jurosTotal;
    
    @Getter
    @Setter
    private Double valorTotal;
    
}