
package br.com.formiga.calc_juros.persistence.dao;


import br.com.formiga.calc_juros.enumeration.OperacaoEnum;
import br.com.formiga.calc_juros.factory.ClienteFactory;
import br.com.formiga.calc_juros.model.Cliente;
import br.com.formiga.calc_juros.persistence.repository.ClienteRepository;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.ejb.Stateless;


/**
 *
 * @author formiga
 */
@Stateless
public class ClienteDAO extends GenericDAO implements ClienteRepository {
    
    @Override
    public void salvar(final Cliente cliente) {
       
        if (Objects.isNull(cliente.getId())) {
            
            super.persistir(cliente, OperacaoEnum.INCLUIR.getOPERACAO());
        } else {
            super.persistir(cliente, OperacaoEnum.ATUALIZAR.getOPERACAO());
        }
    }
    
    @Override
    public void excluir(final Cliente cliente) {
        super.persistir(cliente, OperacaoEnum.EXCLUIR.getOPERACAO());
    }
    
    @Override
    public List<Cliente> listar(final int primeiroRegistro, final int totalDeRegistrosPorPagina, final String campoOrdenacao, final boolean ascendente, final Map<String, Object> filtros) {
        
        final List<Cliente> clientes = ClienteFactory.getClientes();
        
        return clientes;
    }
    
    @Override
    public List<Cliente> listar() {
        
        final List<Cliente> clientes = ClienteFactory.getClientes();
        
        return clientes;
    }
            
}