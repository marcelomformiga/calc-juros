
package br.com.formiga.calc_juros.persistence.dao;


import br.com.formiga.calc_juros.enumeration.OperacaoEnum;
import br.com.formiga.calc_juros.factory.ClienteFactory;
import br.com.formiga.calc_juros.model.Cliente;
import java.util.List;
import java.util.Objects;


/**
 * @author formiga
 * @param <T>
 */
public abstract class GenericDAO {
    
    /**
     * Construtor padrão.
     */
    public GenericDAO() {
        super();
    }

        
    /**
     * Método para incluir, atualizar ou excluir um objeto.
     *
     * @param entidade : T
     * @param operacao : String
     */
    synchronized void persistir(Cliente cliente, final String operacao) {

        if ((Objects.nonNull(cliente)) && (Objects.nonNull(operacao))) {
         
            if (OperacaoEnum.INCLUIR.getOPERACAO().equals(operacao)) {

                Integer id = ClienteFactory.getId();
                id ++;
                
                cliente.setId(id);
                
                ClienteFactory.getClientes().add(cliente);
                
                ClienteFactory.setId(id);
            } else {
                int index = 0;
                
                final List<Cliente> listaAux = ClienteFactory.getClientes();
                
                for (final Cliente clienteAux : listaAux) {
                    
                    if (clienteAux.getId().equals(cliente.getId())) {
                        break;
                    }
                    
                    index ++;
                }
                
                if (OperacaoEnum.ATUALIZAR.getOPERACAO().equals(operacao)) {
                    ClienteFactory.getClientes().get(index).setNome(cliente.getNome());
                    ClienteFactory.getClientes().get(index).setRendimentoMensal(cliente.getRendimentoMensal());
                    ClienteFactory.getClientes().get(index).setRisco(cliente.getRisco());
                    ClienteFactory.getClientes().get(index).setEndereco(cliente.getEndereco());
                } else {
                    ClienteFactory.getClientes().remove(index);
                }
            }
            
        }
    }

}