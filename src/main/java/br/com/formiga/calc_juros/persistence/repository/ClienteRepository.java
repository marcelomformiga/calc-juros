
package br.com.formiga.calc_juros.persistence.repository;


import br.com.formiga.calc_juros.model.Cliente;
import java.util.List;
import java.util.Map;
import javax.ejb.Local;


/**
 *
 * @author formiga
 */
@Local
public interface ClienteRepository
{
 
    abstract void salvar(final Cliente cliente);
    
    abstract void excluir(final Cliente cliente);
    
    abstract List<Cliente> listar(final int primeiroRegistro, final int totalDeRegistrosPorPagina, final String campoOrdenacao, final boolean ascendente, final Map<String, Object> filtros);

    abstract List<Cliente> listar();
    
}