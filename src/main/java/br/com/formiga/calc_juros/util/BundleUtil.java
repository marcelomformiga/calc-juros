
package br.com.formiga.calc_juros.util;


import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import lombok.Getter;


/**
 * Classe utilitária para realizar buscas de chaves no Bundle.
 * 
 * @author formiga
 */
public class BundleUtil {
    
    /**
     * Constante estática que representa o Locale.
     */
    @Getter
    private static final Locale PT_BR = new Locale("pt", "BR");
    
    /**
     * Constante estática representando o arquivo de propriedades do Bundle.
     */
    @Getter
    private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle("/bundle/bundle_pt_BR", BundleUtil.PT_BR);
    

    /**
     * Método estático para buscar uma chave no arquivo Bundle.
     * 
     * @param chave : String
     * @return valor da chave
     */
    public static String recuperarChave(final String chave) {
        
        try {
            return BundleUtil.RESOURCE_BUNDLE.getString(chave);
        } catch(final MissingResourceException missingResourceException) {
            return '!' + chave + '!';
        }
    }
    
}