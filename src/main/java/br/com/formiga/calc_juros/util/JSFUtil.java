
package br.com.formiga.calc_juros.util;


import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import org.apache.commons.lang.StringUtils;


/**
 * Classe estática com métodos utilitários do JSF.
 * 
 * @author formiga
 */
public class JSFUtil {
    
    /**
     * Método estático para recuperar itens selecionados em uma lista.
     * 
     * @param entidades : Lista de objetos selecionados
     * @param selectOne : boolean
     * @return items : Array de SelectItem
     */
    public static SelectItem[] getSelectItems(final List<?> entidades, final boolean selectOne) {      
        
        int size = selectOne ? entidades.size() + 1 : entidades.size();
        
        SelectItem items[] = new SelectItem[size];
        
        int i = 0;
        
        if (selectOne) {
            items[0] = new SelectItem("", "---");
            
            i ++;
        }
        
        for (Object object : entidades) {
            items[i ++] = new SelectItem(object, object.toString());
        }
        
        return items;
    }

    /**
     * Método estático para adicionar uma mensagem de erro na tela com exceção.
     * 
     * @param exception : Exception
     * @param mensagem : String
     */
    public static void informarMensagemDeErroComExcecao(final Exception exception, final String mensagem) {
        
        final String mensagemDaExcecao = exception.getLocalizedMessage();
        
        if (StringUtils.isEmpty(mensagemDaExcecao)) {
            JSFUtil.informarMensagemDeErro(mensagem);
        } else {
            JSFUtil.informarMensagemDeErro(mensagemDaExcecao);
        }
    }

    /**
     * Método estático para adicionar mensagens de erros na tela.
     * 
     * @param mensagens : Lista de Strings
     */
    public static void adicionarMensagensDeErros(final List<String> mensagens) {
        mensagens.forEach((mensagem) -> {
            JSFUtil.informarMensagemDeErro(mensagem);
        });
    }

    /**
     * Método estático para informar a mensagem de erro que será exibida na tela.
     * 
     * @param mensagem : String
     */
    public static void informarMensagemDeErro(final String mensagem) {
        
        final FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, mensagem, mensagem);
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
    }

    /**
     * Método estático para informar na tela uma mensagem de sucesso da operação realizada.
     * 
     * @param mensagem : String
     */
    public static void informarMensagemDeSucesso(final String mensagem) {
        final FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, mensagem, mensagem);
        FacesContext.getCurrentInstance().addMessage("successInfo", facesMessage);
    }

}