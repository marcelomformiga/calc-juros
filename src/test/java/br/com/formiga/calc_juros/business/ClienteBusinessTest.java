
package br.com.formiga.calc_juros.business;


import br.com.formiga.calc_juros.enumeration.RiscoEnum;
import br.com.formiga.calc_juros.model.Cliente;
import br.com.formiga.calc_juros.model.dto.EmprestimoDTO;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Testes da classe ClienteBusiness para validar as regras de negócio da aplicação.
 * 
 * @author formiga
 */
@RunWith(MockitoJUnitRunner.class)
public class ClienteBusinessTest {
    
    @InjectMocks
    private ClienteBusiness clienteBusiness;
    
    
    @BeforeClass
    public static void setUpClass() {
        System.out.println("***** INÍCIO *****");
        System.out.println();
    }
    
    @AfterClass
    public static void tearDownClass() {
        System.out.println();
        System.out.println("***** FIM *****");
    }
    
    @Before
    public void setUp() {
        System.out.println();
    }
    
    @After
    public void tearDown() {
        System.out.println();
        System.out.println("---------------------------------------------------");
    }

    
    /**
     * Teste para mapear tipo de risco A.
     */
    @Test
    public void testMapearRisco_RiscoA() {
        
        System.out.println("testMapearRisco_RiscoA");
        
        Cliente cliente = new Cliente();
        cliente.setId(1);
        cliente.setNome("Marcelo");
        cliente.setRendimentoMensal(10000.00);

        final Character resultado = this.clienteBusiness.mapearRisco(cliente);
        
        Assert.assertNotNull("[1]Resultado não pode ser nulo", resultado);
        Assert.assertEquals("[2]Objetos devem ser iguais", RiscoEnum.A.getRISCO(), resultado);
    }
    
    /**
     * Teste para mapear tipo de risco B.
     */
    @Test
    public void testMapearRisco_RiscoB() {
        
        System.out.println("testMapearRisco_RiscoB");
        
        Cliente cliente = new Cliente();
        cliente.setId(1);
        cliente.setNome("Marcelo");
        cliente.setRendimentoMensal(8000.00);

        final Character resultado = this.clienteBusiness.mapearRisco(cliente);
        
        Assert.assertNotNull("[1]Resultado não pode ser nulo", resultado);
        Assert.assertEquals("[2]Objetos devem ser iguais", RiscoEnum.B.getRISCO(), resultado);
    }
    
    /**
     * Teste para mapear tipo de risco C.
     */
    @Test
    public void testMapearRisco_RiscoC() {
        
        System.out.println("testMapearRisco_RiscoC");
        
        Cliente cliente = new Cliente();
        cliente.setId(1);
        cliente.setNome("Marcelo");
        cliente.setRendimentoMensal(2000.00);

        final Character resultado = this.clienteBusiness.mapearRisco(cliente);
        
        Assert.assertNotNull("[1]Resultado não pode ser nulo", resultado);
        Assert.assertEquals("[2]Objetos devem ser iguais", RiscoEnum.C.getRISCO(), resultado);
    }
    
    /**
     * Teste para mapear porém sem informar rendimento mensal do Cliente.
     */
    @Test
    public void testMapearRisco_SemRendimentoMensal() {
        
        System.out.println("testMapearRisco_SemRendimentoMensal");
        
        Cliente cliente = new Cliente();
        cliente.setId(1);
        cliente.setNome("Marcelo");

        final Character resultado = this.clienteBusiness.mapearRisco(cliente);
        
        Assert.assertNull("[1]Resultado deve ser nulo", resultado);
        Assert.assertEquals("[2]Objetos devem ser iguais", null, resultado);
    }
    
    /**
     * Teste para recuperar a taxa de juros mensal, conforme risco do tipo A.
     */
    @Test
    public void testRecuperarTaxa_RiscoA() {
        
        System.out.println("testRecuperarTaxa_RiscoA");
        
        Cliente cliente = new Cliente();
        cliente.setId(1);
        cliente.setNome("Marcelo");
        cliente.setRendimentoMensal(10000.00);
        
        final Character risco = this.clienteBusiness.mapearRisco(cliente);
        
        cliente.setRisco(risco);

        final Double resultado = this.clienteBusiness.recuperarTaxa(cliente);
        
        Assert.assertNotNull("[1]Resultado não pode ser nulo", resultado);
        Assert.assertEquals("[2]Objetos devem ser iguais", RiscoEnum.A.getRISCO(), risco);
        Assert.assertEquals("[3]Objetos devem ser iguais", RiscoEnum.A.getTAXA(), resultado);
    }
    
    /**
     * Teste para recuperar a taxa de juros mensal, conforme risco do tipo B.
     */
    @Test
    public void testRecuperarTaxa_RiscoB() {
        
        System.out.println("testRecuperarTaxa_RiscoB");
        
        Cliente cliente = new Cliente();
        cliente.setId(1);
        cliente.setNome("Marcelo");
        cliente.setRendimentoMensal(8000.00);
        
        final Character risco = this.clienteBusiness.mapearRisco(cliente);
        
        cliente.setRisco(risco);

        final Double resultado = this.clienteBusiness.recuperarTaxa(cliente);
        
        Assert.assertNotNull("[1]Resultado não pode ser nulo", resultado);
        Assert.assertEquals("[2]Objetos devem ser iguais", RiscoEnum.B.getRISCO(), risco);
        Assert.assertEquals("[3]Objetos devem ser iguais", RiscoEnum.B.getTAXA(), resultado);
    }

    /**
     * Teste para recuperar a taxa de juros mensal, conforme risco do tipo C.
     */
    @Test
    public void testRecuperarTaxa_RiscoC() {
        
        System.out.println("testRecuperarTaxa_RiscoC");
        
        Cliente cliente = new Cliente();
        cliente.setId(1);
        cliente.setNome("Marcelo");
        cliente.setRendimentoMensal(2000.00);
        
        final Character risco = this.clienteBusiness.mapearRisco(cliente);
        
        cliente.setRisco(risco);

        final Double resultado = this.clienteBusiness.recuperarTaxa(cliente);
        
        Assert.assertNotNull("[1]Resultado não pode ser nulo", resultado);
        Assert.assertEquals("[2]Objetos devem ser iguais", RiscoEnum.C.getRISCO(), risco);
        Assert.assertEquals("[3]Objetos devem ser iguais", RiscoEnum.C.getTAXA(), resultado);
    }
    
    /**
     * Teste para recuperar a taxa de juros de juros conforme o risco porém sem informar o risco.
     */
    @Test
    public void testRecuperarTaxa_SemRiscoInformado() {
        
        System.out.println("testRecuperarTaxa_SemRiscoInformado");
        
        Cliente cliente = new Cliente();
        cliente.setId(1);
        cliente.setNome("Marcelo");
        cliente.setRendimentoMensal(2000.00);

        final Double resultado = this.clienteBusiness.recuperarTaxa(cliente);
        
        Assert.assertNull("[1]Resultado deve ser nulo", resultado);
        Assert.assertEquals("[2]Objetos devem ser iguais", null, resultado);
    }
    
    /**
     * Teste do método calcular, calculando com a taxa de risco A.
     */
    @Test
    public void testCalcular_RiscoA() {
        
        System.out.println("testCalcular_RiscoA");
        
        EmprestimoDTO emprestimo = new EmprestimoDTO();
        emprestimo.setValorSolicitado(1000.00);
        emprestimo.setTaxa(RiscoEnum.A.getTAXA());
        emprestimo.setTempo(6);
        
        final EmprestimoDTO resultado = this.clienteBusiness.calcular(emprestimo);
        
        Assert.assertNotNull("[1]Resultado não pode ser nulo", resultado);
        Assert.assertEquals("[2]Objetos devem ser iguais", emprestimo.getValorSolicitado(), resultado.getValorSolicitado());
        Assert.assertEquals("[3]Objetos devem ser iguais", emprestimo.getTaxa(), resultado.getTaxa());
        Assert.assertEquals("[4]Objetos devem ser iguais", emprestimo.getTempo(), resultado.getTempo());
        Assert.assertTrue("[5]Juros do mês deve ser R$19,00", 19.00 == resultado.getJurosMes());
        Assert.assertTrue("[6]Juros total do empréstimo deve ser R$114,00", 114.00 == resultado.getJurosTotal());
        Assert.assertTrue("[7]Valor total do empréstimo deve ser R$1114,00", 1114.00 == resultado.getValorTotal());
    }
    
    /**
     * Teste do método calcular, calculando com a taxa de risco B.
     */
    @Test
    public void testCalcular_RiscoB() {
        
        System.out.println("testCalcular_RiscoB");
        
        EmprestimoDTO emprestimo = new EmprestimoDTO();
        emprestimo.setValorSolicitado(1000.00);
        emprestimo.setTaxa(RiscoEnum.B.getTAXA());
        emprestimo.setTempo(6);
        
        final EmprestimoDTO resultado = this.clienteBusiness.calcular(emprestimo);
        
        Assert.assertNotNull("[1]Resultado não pode ser nulo", resultado);
        Assert.assertEquals("[2]Objetos devem ser iguais", emprestimo.getValorSolicitado(), resultado.getValorSolicitado());
        Assert.assertEquals("[3]Objetos devem ser iguais", emprestimo.getTaxa(), resultado.getTaxa());
        Assert.assertEquals("[4]Objetos devem ser iguais", emprestimo.getTempo(), resultado.getTempo());
        Assert.assertTrue("[5]Juros do mês deve ser R$19,00", 50.00 == resultado.getJurosMes());
        Assert.assertTrue("[6]Juros total do empréstimo deve ser R$300,00", 300.00 == resultado.getJurosTotal());
        Assert.assertTrue("[7]Valor total do empréstimo deve ser R$1300,00", 1300.00 == resultado.getValorTotal());
    }
    
    /**
     * Teste do método calcular, calculando com a taxa de risco C.
     */
    @Test
    public void testCalcular_RiscoC() {
        
        System.out.println("testCalcular_RiscoC");
        
        EmprestimoDTO emprestimo = new EmprestimoDTO();
        emprestimo.setValorSolicitado(1000.00);
        emprestimo.setTaxa(RiscoEnum.C.getTAXA());
        emprestimo.setTempo(6);
        
        final EmprestimoDTO resultado = this.clienteBusiness.calcular(emprestimo);
        
        Assert.assertNotNull("[1]Resultado não pode ser nulo", resultado);
        Assert.assertEquals("[2]Objetos devem ser iguais", emprestimo.getValorSolicitado(), resultado.getValorSolicitado());
        Assert.assertEquals("[3]Objetos devem ser iguais", emprestimo.getTaxa(), resultado.getTaxa());
        Assert.assertEquals("[4]Objetos devem ser iguais", emprestimo.getTempo(), resultado.getTempo());
        Assert.assertTrue("[5]Juros do mês deve ser R$19,00", 100.00 == resultado.getJurosMes());
        Assert.assertTrue("[6]Juros total do empréstimo deve ser R$300,00", 600.00 == resultado.getJurosTotal());
        Assert.assertTrue("[7]Valor total do empréstimo deve ser R$1300,00", 1600.00 == resultado.getValorTotal());
    }
    
}